package com.funfoxstudios.framework.core.background;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;

public class VerticalParallaxBackground extends ParallaxBackground {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// FIELDS
	// ===============================================================

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public VerticalParallaxBackground(ParallaxLayer[] layers, OrthographicCamera pCamera, Vector2 speed) {
		super(layers, pCamera, speed);
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
		@Override
		public void draw(Batch batch, float delta) {
			if (mBatch == null) {
				mBatch = batch;
			}

			this.camera.position.add(speed.x * delta, speed.y * delta, 0);
			for (ParallaxLayer layer : layers) {
				mBatch.setProjectionMatrix(camera.projection);
				mBatch.begin();
				float currentY = -camera.position.y * layer.getParallaxRatio().y % (layer.getHeigth() + layer.getPadding().y);

				if (layer.isRepeatable()) {

					if (speed.y < 0) {
						currentY += -(layer.getHeigth() + layer.getPadding().y);
					}
					do {
						float currentX = -camera.position.x * layer.getParallaxRatio().x % (layer.getWidth() + layer.getPadding().x);
						if (speed.x < 0) {
							currentX += -(layer.getWidth() + layer.getPadding().x);
						}
						do {
							mBatch.draw(layer.getRegion(),
									-this.camera.viewportWidth / 2  + layer.getStartPosition().x,
									-this.camera.viewportHeight / 2 - currentY + layer.getStartPosition().y, layer.getWidth(), layer.getHeigth());
							currentX += (layer.getWidth() + layer.getPadding().x);
						} while (currentX < camera.viewportWidth);
						currentY += (layer.getHeigth() + layer.getPadding().y);
					} while (currentY < camera.viewportHeight);
				} else {
					mBatch.draw(layer.getRegion(),
							-this.camera.viewportWidth / 2  + layer.getStartPosition().x,
							-this.camera.viewportHeight / 2 + currentY + layer.getStartPosition().y, layer.getWidth(), layer.getHeigth());
				}
				mBatch.end();
			}
		}

	// ===============================================================
	// METHODS
	// ===============================================================

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}
