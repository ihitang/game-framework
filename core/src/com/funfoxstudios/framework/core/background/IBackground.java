package com.funfoxstudios.framework.core.background;

import com.badlogic.gdx.graphics.g2d.Batch;

public interface IBackground {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// METHODS
	// ===============================================================
	void update(float delta);

	void draw(Batch pBatch, float delta);
}
