package com.funfoxstudios.framework.core.background;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class ParallaxBackground implements ITouchBackground {

	// sample. how to use
	/*ParallaxLayer baseBackground = new ParallaxLayer(getCustomTextureRegion("bg_game_base"), width / 1.5f, height / 1.5f, new Vector2(0.15f, 0.0f), new Vector2(0, height / 2f), new Vector2(0, 0));
	ParallaxLayer baseMiddleBackground = new ParallaxLayer(getCustomTextureRegion("bg_game_middle"), width, height, new Vector2(0.45f, 0.0f), new Vector2(0, height / 3f), new Vector2(0, 0));
	//ParallaxLayer baseNearBackground = new ParallaxLayer(getCustomTextureRegion("bg_game_near"), width, height, new Vector2(0.3f, 0), new Vector2(0, 0), new Vector2(0, 0));
	ParallaxBackground gameBackground = new ParallaxBackground(new ParallaxLayer[]{baseBackground, baseMiddleBackground}, pCamera, new Vector2(0, 0));*/

	protected ParallaxLayer[] layers;
	protected Camera camera;
	protected Vector2 speed = new Vector2();
	protected Batch mBatch;

	/**
	 * @param layers The  background layers
	 * @param speed  A Vector2 attribute to point out the x and y speed
	 */
	public ParallaxBackground(ParallaxLayer[] layers, OrthographicCamera pCamera, Vector2 speed) {
		this.layers = layers;
		this.speed.set(speed);
		camera = pCamera;
		if (camera == null) {
			camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			mBatch = new SpriteBatch();
		}
	}

	@Override
	public void draw(Batch batch, float delta) {
		if (mBatch == null) {
			mBatch = batch;
		}

		this.camera.position.add(speed.x * delta, speed.y * delta, 0);
		for (ParallaxLayer layer : layers) {
			mBatch.setProjectionMatrix(camera.projection);
			mBatch.begin();
			float currentX = -camera.position.x * layer.getParallaxRatio().x % (layer.getWidth() + layer.getPadding().x);

			if (layer.isRepeatable()) {

				if (speed.x < 0) {
					currentX += -(layer.getWidth() + layer.getPadding().x);
				}
				do {
					float currentY = -camera.position.y * layer.getParallaxRatio().y % (layer.getHeigth() + layer.getPadding().y);
					if (speed.y < 0) {
						currentY += -(layer.getHeigth() + layer.getPadding().y);
					}
					do {
						mBatch.draw(layer.getRegion(),
								-this.camera.viewportWidth / 2 + currentX + layer.getStartPosition().x,
								-this.camera.viewportHeight / 2 + layer.getStartPosition().y, layer.getWidth(), layer.getHeigth());
						currentY += (layer.getHeigth() + layer.getPadding().y);
					} while (currentY < camera.viewportHeight);
					currentX += (layer.getWidth() + layer.getPadding().x);
				} while (currentX < camera.viewportWidth);
			} else {
				mBatch.draw(layer.getRegion(),
						-this.camera.viewportWidth / 2 + currentX + layer.getStartPosition().x,
						-this.camera.viewportHeight / 2 + layer.getStartPosition().y, layer.getWidth(), layer.getHeigth());
			}
			mBatch.end();
		}
	}

	@Override
	public void setVelocityX(float pX) {
		speed.x = pX;
	}

	@Override
	public void setVelocityY(float pY) {
		speed.y = pY;
	}

	public float getVelocityX() {
		return speed.x;
	}

	public float getVelocityY() {
		return speed.y;
	}

	@Override
	public void update(float delta) {

	}
}