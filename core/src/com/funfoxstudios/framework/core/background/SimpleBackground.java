package com.funfoxstudios.framework.core.background;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class SimpleBackground extends Rectangle implements ITouchBackground {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// FIELDS
	// ===============================================================
	private TextureRegion mTextureRegion;
	private float mX, mY, mWidth, mHeight;

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public SimpleBackground(TextureRegion textureRegion) {
		this(textureRegion, 0.0f, 0.0f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}

	public SimpleBackground(TextureRegion texture, float x, float y, float width, float height) {
		mTextureRegion = texture;
		mX = x;
		mY = y;
		mWidth = width;
		mHeight = height;
	}

	public SimpleBackground(Texture texture, float x, float y, float width, float height) {
		this(new TextureRegion(texture), x, y, width, height);
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	public void update(float delta) {

	}

	@Override
	public void draw(Batch batch, float delta) {
		batch.begin();
		batch.draw(mTextureRegion, mX, mY, mWidth, mHeight);
		batch.end();
	}

	@Override
	public void setVelocityX(float x) {

	}

	@Override
	public void setVelocityY(float y) {

	}

	// ===============================================================
	// METHODS
	// ===============================================================

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}
