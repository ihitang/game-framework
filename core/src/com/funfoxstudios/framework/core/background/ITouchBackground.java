package com.funfoxstudios.framework.core.background;

public interface ITouchBackground extends IBackground {

	void setVelocityX(float pX);

	void setVelocityY(float pY);
}
