package com.funfoxstudios.framework.core.background;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class ParallaxLayer {
	private TextureRegion region;
	private Vector2 parallaxRatio;
	private Vector2 startPosition;
	private Vector2 padding;
	private float mWidth;
	private float mHeigth;
	private boolean isRepeatable;

	/**
	 * @param region        the TextureRegion to draw , this can be any width/height
	 * @param parallaxRatio the relative speed of x,y {@link ParallaxBackground#ParallaxBackground(ParallaxLayer[], float, float, Vector2)}
	 * @param startPosition the init position of x,y
	 * @param padding       the padding of the region at x,y
	 */
	public ParallaxLayer(TextureRegion region, float pWidth, float pHeight, Vector2 parallaxRatio, Vector2 startPosition, Vector2 padding) {
		this.region = region;
		setParallaxRatio(parallaxRatio);
		setStartPosition(startPosition);
		setPadding(padding);
		setWidth(pWidth);
		setHeigth(pHeight);
		isRepeatable = true;
	}

	public void setRepeatable(boolean isRepeatable) {
		this.isRepeatable = isRepeatable;
	}

	public boolean isRepeatable() {
		return this.isRepeatable;
	}

	public TextureRegion getRegion() {
		return region;
	}

	public Vector2 getParallaxRatio() {
		return parallaxRatio;
	}

	public void setParallaxRatio(Vector2 parallaxRatio) {
		this.parallaxRatio = parallaxRatio;
	}

	public Vector2 getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(Vector2 startPosition) {
		this.startPosition = startPosition;
	}

	public Vector2 getPadding() {
		return padding;
	}

	public void setPadding(Vector2 padding) {
		this.padding = padding;
	}

	public float getWidth() {
		return mWidth;
	}

	public void setWidth(float pWidth) {
		this.mWidth = pWidth;
	}

	public float getHeigth() {
		return mHeigth;
	}

	public void setHeigth(float pHeigth) {
		this.mHeigth = pHeigth;
	}
}