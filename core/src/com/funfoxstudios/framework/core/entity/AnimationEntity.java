package com.funfoxstudios.framework.core.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.funfoxstudios.framework.core.entity.listeners.OnClickListener;

public class AnimationEntity extends TextureRegionEntity {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// FIELDS
	// ===============================================================
	private Animation mAnimation;
	private float mStateTime;

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public AnimationEntity(float x, float y, float width, float height, float frameDurationSec, Animation.PlayMode playMode, Array<? extends TextureRegion> keyFrames) {
		super(null, x, y, width, height);
		mAnimation = new Animation(frameDurationSec, keyFrames, playMode);
		mStateTime = 0.0f;
	}

	public AnimationEntity(float x, float y, float width, float height, float frameDurationSec, Animation.PlayMode playMode, OnClickListener pOnClickListener, Array<? extends TextureRegion> keyFrames) {
		super(null, x, y, width, height, pOnClickListener);
		mAnimation = new Animation(frameDurationSec, keyFrames, playMode);
		mStateTime = 0.0f;
	}

	public AnimationEntity(float x, float y, float width, float height, float frameDurationSec, Animation.PlayMode playMode, TextureRegion... keyFrames) {
		super(null, x, y, width, height);
		mAnimation = new Animation(frameDurationSec, keyFrames);
		mAnimation.setPlayMode(playMode);
		mStateTime = 0.0f;
	}

	public AnimationEntity(float x, float y, float width, float height, float frameDurationSec, Animation.PlayMode playMode, OnClickListener pOnClickListener, TextureRegion... keyFrames) {
		super(null, x, y, width, height, pOnClickListener);
		mAnimation = new Animation(frameDurationSec, keyFrames);
		mAnimation.setPlayMode(playMode);
		mStateTime = 0.0f;
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	public void draw(SpriteBatch batch) {
		mStateTime += Gdx.graphics.getDeltaTime();
		setBackground(mAnimation.getKeyFrame(mStateTime));
		super.draw(batch);
	}

	// ===============================================================
	// METHODS
	// ===============================================================

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}
