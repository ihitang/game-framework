package com.funfoxstudios.framework.core.entity;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.funfoxstudios.framework.core.entity.listeners.OnClickListener;

public class ProgressBarEntity extends TextureRegionEntity {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// FIELDS
	// ===============================================================
	private TextureRegionEntity mFrontEntity;
	private float mValue;
	private float mMaxValue;
	private boolean mAxisX;
	private float mPadding;

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public ProgressBarEntity(TextureRegion background, TextureRegion front, boolean axisX, float currentValue, float maxValue, float padding, float x, float y, float width, float height, OnClickListener onClickListener) {
		super(background, x, y, width, height, onClickListener);

		mFrontEntity = new TextureRegionEntity(front, padding, padding, width - padding, height - padding * 3);
		mAxisX = axisX;
		mPadding = padding;
		setMaxValue(maxValue);
		setValue(currentValue);
		addChild(mFrontEntity);
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================
	public float getValue() {
		return mValue;
	}

	public void setValue(float value) {
		if (value < 0.0f || value > mMaxValue)
			return;

		mValue = value;
		if (mAxisX) {
			mFrontEntity.setWidth((getWidth() - mPadding) * value / mMaxValue);
		} else {
			mFrontEntity.setHeight((getHeight() - mPadding) * value / mMaxValue);
		}
	}

	public float getMaxValue() {
		return mMaxValue;
	}

	public void setMaxValue(float maxValue) {
		if (maxValue < 0.0f)
			return;

		mMaxValue = maxValue;
	}

	protected IEntity getProgressBarEntity() {
		return getChild(0);
	}

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	public Rectangle setWidth(float width) {
		getProgressBarEntity().setWidth(width - mPadding * 2);
		setValue(getValue());
		return super.setWidth(width);
	}

	@Override
	public Rectangle setHeight(float height) {
		getProgressBarEntity().setHeight(height - mPadding * 2);
		setValue(getValue());
		return super.setHeight(height);
	}

	// ===============================================================
	// METHODS
	// ===============================================================

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}