package com.funfoxstudios.framework.core.entity.listeners;

public interface OnClickListener {

	enum Types {Click, TouchDown, TouchUp}

	void onClick();

	void onTouchDown();

	void onTouchUp();

}
