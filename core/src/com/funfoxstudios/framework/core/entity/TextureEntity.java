package com.funfoxstudios.framework.core.entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.funfoxstudios.framework.core.entity.listeners.OnClickListener;

public class TextureEntity extends BaseEntity<Texture> {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// FIELDS
	// ===============================================================

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public TextureEntity(Texture pBackground, float pX, float pY, float pWidth, float pHeight) {
		super(pBackground, pX, pY, pWidth, pHeight);
	}

	public TextureEntity(Texture pBackground, float pX, float pY, float pWidth, float pHeight, OnClickListener pOnClickListener) {
		super(pBackground, pX, pY, pWidth, pHeight, pOnClickListener);
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	public void drawBackground(SpriteBatch pBatch, Texture pBackground) {
		pBatch.begin();
		pBatch.draw(pBackground, getX(), getY(), getWidth(), getHeight());
		pBatch.end();
	}

	// ===============================================================
	// METHODS
	// ===============================================================

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}