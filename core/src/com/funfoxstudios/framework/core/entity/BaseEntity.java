package com.funfoxstudios.framework.core.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.funfoxstudios.framework.core.entity.listeners.OnClickListener;

import java.util.ArrayList;

public abstract class BaseEntity<T> extends Rectangle implements IEntity<T> {

	// ===============================================================
	// CONSTANTS
	// ===============================================================
	private static final long serialVersionUID = 1L;

	// ===============================================================
	// FIELDS
	// ===============================================================
	private transient T mBackground;
	private float mAngle;
	private float mScale;
	private boolean mIsVisible;
	private float mOriginX;
	private float mOriginY;
	private OnClickListener mOnClickListener;
	private boolean mIsEnabled;
	private ArrayList<IEntity> mChildren = new ArrayList<IEntity>();

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public BaseEntity(T background, float x, float y, float width, float height) {
		super(x, y, width, height);
		this.width = width;
		this.height = height;
		mBackground = background;
		mAngle = 0.0f;
		mScale = 1.0f;
		mIsVisible = true;
		mIsEnabled = true;

		mOriginX = this.width / 2.0f;
		mOriginY = this.height / 2.0f;
	}

	public BaseEntity(T pBackground, float x, float y, float width, float height, OnClickListener onClickListener) {
		this(pBackground, x, y, width, height);

		mOnClickListener = onClickListener;
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	public BaseEntity<T> setOnClickListener(OnClickListener onClickListener) {
		if (onClickListener == null) {
			throw new IllegalArgumentException("OnClickListener can't be null.");
		}

		mOnClickListener = onClickListener;

		return this;
	}

	@Override
	public boolean isClickable() {
		return mOnClickListener != null && isEnabled();
	}

	@Override
	public Rectangle setY(float y) {
		setPositionY(y);
		return super.setY(y);
	}

	@Override
	public Rectangle setX(float x) {
		setPositionX(x);
		return super.setX(x);
	}

	@Override
	public BaseEntity<T> setPosition(float x, float y) {
		if (mChildren != null) {
			if (mChildren.size() > 0) {
				for (IEntity entity : mChildren) {
					entity.setPosition(entity.getX() - this.x + x, entity.getY() - this.y + y);
				}
			}
		}
		this.x = x;
		this.y = y;

		return this;
	}

	@Override
	public BaseEntity<T> setPositionY(float y) {
		if (mChildren != null) {
			if (mChildren.size() > 0) {
				for (IEntity entity : mChildren) {
					entity.setY(entity.getY() - this.y + y);
				}
			}
		}
		this.y = y;

		return this;
	}

	@Override
	public BaseEntity<T> setPositionX(float x) {
		if (mChildren != null) {
			if (mChildren.size() > 0) {
				for (IEntity entity : mChildren) {
					entity.setX(entity.getX() - this.x + x);
				}
			}
		}
		this.x = x;

		return this;
	}

	@Override
	public float getAngle() {
		return mAngle;
	}

	@Override
	public BaseEntity<T> setAngle(float angle) {
		mAngle = angle;
		if (mChildren != null) {
			if (mChildren.size() > 0) {
				for (IEntity entity : mChildren) {
					entity.setAngle(angle);
				}
			}
		}

		return this;
	}

	@Override
	public float getScale() {
		return mScale;
	}

	@Override
	public BaseEntity<T> setScale(float value) {
		mScale = value;

		return this;
	}

	@Override
	public boolean getVisible() {
		return mIsVisible;
	}

	@Override
	public BaseEntity<T> setVisible(boolean visible) {
		mIsVisible = visible;

		if (mChildren != null) {
			if (mChildren.size() > 0) {
				for (IEntity entity : mChildren) {
					entity.setVisible(visible);
				}
			}
		}

		return this;
	}

	@Override
	public IEntity getChild(int childId) throws NullPointerException {
		return mChildren.get(childId);
	}

	@Override
	public ArrayList<IEntity> getChildren() {
		return mChildren;
	}

	@Override
	public int addChild(IEntity entity) {
		mChildren.add(entity);
		return mChildren.indexOf(entity);
	}

	@Override
	public BaseEntity<T> setBackground(T background) {
		mBackground = background;
		return this;
	}

	@Override
	public float getOriginX() {
		return mOriginX;
	}

	@Override
	public BaseEntity<T> setOriginX(float originX) {
		this.mOriginX = originX;
		if (mChildren != null) {
			if (mChildren.size() > 0) {
				for (IEntity entity : mChildren) {
					entity.setOriginX(originX - entity.getX());
				}
			}
		}

		return this;
	}

	@Override
	public float getOriginY() {
		return mOriginY;
	}

	@Override
	public BaseEntity<T> setOriginY(float originY) {
		this.mOriginY = originY;
		if (mChildren != null) {
			if (mChildren.size() > 0) {
				for (IEntity entity : mChildren) {
					entity.setOriginY(originY - entity.getY());
				}
			}
		}

		return this;
	}

	@Override
	public BaseEntity<T> setOrigin(float originX, float originY) {
		setOriginX(originX);
		setOriginY(originY);

		return this;
	}

	@Override
	public boolean isEnabled() {
		return mIsEnabled;
	}

	public BaseEntity<T> setEnabled(boolean isEnabled) {
		mIsEnabled = isEnabled;

		for (IEntity entity : mChildren) {
			entity.setEnabled(isEnabled);
		}

		return this;
	}

	@Override
	public void onClick() {
		if (mOnClickListener != null && isEnabled()) {
			mOnClickListener.onClick();
		}
	}

	@Override
	public void onTouchDown() {
		if (mOnClickListener != null && isEnabled()) {
			mOnClickListener.onTouchDown();
		}
	}

	@Override
	public void onTouchUp() {
		if (mOnClickListener != null && isEnabled()) {
			mOnClickListener.onTouchDown();
		}
	}

	// ===============================================================
	// METHODS
	// ===============================================================
	public void draw(SpriteBatch batch) {
		if (mIsVisible) {
			if (mBackground != null) {
				drawBackground(batch, mBackground);
			}

			if (mChildren != null) {
				for (int i = 0; i < mChildren.size(); i++) {
					IEntity entity = mChildren.get(i);
					if (entity.getVisible()) {
						entity.draw(batch);
					}
				}
			}
		}
	}

	protected abstract void drawBackground(SpriteBatch batch, T background);

	public T getBackground() {
		return mBackground;
	}

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}