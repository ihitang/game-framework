package com.funfoxstudios.framework.core.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.funfoxstudios.framework.core.entity.listeners.OnClickListener;

public class TextureRegionEntity extends BaseEntity<TextureRegion> {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// FIELDS
	// ===============================================================

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public TextureRegionEntity(TextureRegion pBackground, float pX, float pY, float pWidth, float pHeight) {
		super(pBackground, pX, pY, pWidth, pHeight);
	}

	public TextureRegionEntity(TextureRegion pBackground, float pX, float pY, float pWidth, float pHeight, OnClickListener pOnClickListener) {
		super(pBackground, pX, pY, pWidth, pHeight, pOnClickListener);
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	public void drawBackground(SpriteBatch pBatch, TextureRegion pBackground) {
		pBatch.begin();
		pBatch.draw(pBackground, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScale(), getScale(), getAngle());
		pBatch.end();
	}

	// ===============================================================
	// METHODS
	// ===============================================================

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}