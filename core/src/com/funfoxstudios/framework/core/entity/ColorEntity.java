package com.funfoxstudios.framework.core.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class ColorEntity extends TextureRegionEntity {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// FIELDS
	// ===============================================================
	private Color mColor;
	private ShapeRenderer mRender;

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================

	public ColorEntity(Color color, float x, float y, float width, float height) {
		super(null, x, y, width, height);
		mColor = color;
		mRender = new ShapeRenderer();
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================
	public Color getColor() {
		return mColor;
	}

	public void setColor(Color color) {
		this.mColor = color;
	}

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================

	// ===============================================================
	// METHODS
	// ===============================================================

	@Override
	public void draw(SpriteBatch pBatch) {
		if (getVisible()) {
			Gdx.gl.glEnable(GL20.GL_BLEND);
			mRender.setColor(mColor.r, mColor.g, mColor.b, mColor.a);

			mRender.begin(ShapeRenderer.ShapeType.Filled);
			mRender.rect(x, y, width, height, mColor, mColor, mColor, mColor);
			mRender.end();

			Gdx.gl.glDisable(GL20.GL_BLEND);
		}
		super.draw(pBatch);
	}

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}
