package com.funfoxstudios.framework.core.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.funfoxstudios.framework.core.entity.listeners.OnClickListener;

import java.util.ArrayList;

public interface IEntity<T> extends OnClickListener {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// METHODS
	// ===============================================================
	IEntity<T> setOnClickListener(OnClickListener pOnClickListener);

	IEntity<T> setPosition(float x, float y);

	float getX();

	float getY();

	Rectangle setX(float pX);

	Rectangle setY(float pY);

	float getWidth();

	float getHeight();

	Rectangle setWidth(float pWidth);

	Rectangle setHeight(float pHeight);

	IEntity<T> setPositionX(float pX);

	IEntity<T> setPositionY(float pY);

	float getOriginX();

	float getOriginY();

	IEntity<T> setOrigin(float originX, float originY);

	IEntity<T> setOriginX(float pOriginX);

	IEntity<T> setOriginY(float pOriginY);

	float getAngle();

	IEntity<T> setAngle(float pAngle);

	float getScale();

	IEntity<T> setScale(float pScale);

	boolean getVisible();

	IEntity<T> setVisible(boolean pIsVisible);

	IEntity getChild(int childId);

	ArrayList<IEntity> getChildren();

	int addChild(IEntity pEntity);

	IEntity<T> setBackground(T pTextureRegion);

	boolean isEnabled();

	IEntity<T> setEnabled(boolean pEnabled);

	void draw(SpriteBatch pBatch);

	T getBackground();

	boolean isClickable();

}
