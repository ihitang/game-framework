package com.funfoxstudios.framework;

import com.badlogic.gdx.audio.Music;

public interface SoundController {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// METHODS
	// ===============================================================
	boolean playMusic(Music music);

	void stopMusic(Music music);

	boolean isMute();
}
