package com.funfoxstudios.framework.utils;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.util.List;

public class AnimationDescription {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// FIELDS
	// ===============================================================
	private List<String> mTextures;
	private float mFrameDurationSec;
	private Animation.PlayMode mPlayMode;
	private Animation mAnimation;
	private final String mAnimationPath;
	private final String mFirstFrame;

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public AnimationDescription(String animationPath, String firstFrame, float frameDurationSec, Animation.PlayMode playMode) {
		mAnimationPath = animationPath;
		mFirstFrame = firstFrame;
		mFrameDurationSec = frameDurationSec;
		mPlayMode = playMode;
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================
	public List<String> getTextureRegions() {
		return mTextures;
	}

	public float getFrameDurationSec() {
		return mFrameDurationSec;
	}

	public Animation.PlayMode getPlayMode() {
		return mPlayMode;
	}

	public Animation getAnimation() {
		return mAnimation;
	}

	/**
	 * Should calls when Assets loaded
	 *
	 * @see com.funfoxstudios.framework.resources.Assets
	 * @see com.funfoxstudios.framework.resources.AbstractAssets
	 */
	public void prepareAnimation(Array<TextureRegion> textureRegions) {

		mAnimation = new Animation(mFrameDurationSec, textureRegions, mPlayMode);
	}

	public boolean isAnimationPrepared() {
		return mAnimation != null;
	}

	public String getAnimationPath() {
		return mAnimationPath;
	}

	public void setTextureRegions(List<String> textureRegions) {
		this.mTextures = textureRegions;
	}

	public String getFirstFrame() {
		return mFirstFrame;
	}

	public void clear() {
		mTextures.clear();
		mTextures = null;
		mAnimation = null;
	}

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================

	// ===============================================================
	// METHODS
	// ===============================================================

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}
