package com.funfoxstudios.framework.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Disposable;
import com.funfoxstudios.framework.utils.AnimationDescription;

public interface Assets extends Disposable {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// METHODS
	// ===============================================================

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

	/**
	 * You should provide your own implementation of {@link AssetManager}
	 * You can extend it from {@link AbstractAssets}
	 *
	 * @return your implementation of {@link AssetManager
	 */
	AssetManager getAssetManager();

	/**
	 * Loading resources related to the specific screen
	 */
	void loadScreenResources(int screenId);

	/**
	 * Loading assets for current game level
	 *
	 * @see com.funfoxstudios.framework.levels.LevelManager
	 */
	void loadAssetsForCurrentGameLevel();

	/**
	 * Loading assets based on {@link AnimationDescription}
	 *
	 * @see com.funfoxstudios.framework.levels.LevelManager
	 * @see AnimationDescription
	 */
	void loadAnimation(AnimationDescription animationDescription);

	/**
	 * Loading custom resource
	 */
	void loadResource(String resourcePath, Class resourceClass);

	/**
	 * UnLoading custom resource
	 */
	void unloadResources(String resourcePath);

	/**
	 * UnLoading resources related to the specific screen
	 */
	void unloadScreenResources(int screenId);

	/**
	 * Get preload assets from {@link AssetManager}
	 */
	<T> T getAsset(String fileName);

	/**
	 * Assets Loading progress in percents. value 100 - loading completed
	 *
	 * @return progress value from 0 to 100
	 * @see AssetManager#getProgress()
	 */
	float getProgress();

	void setCancelLoading(boolean cancelLoading);
}
