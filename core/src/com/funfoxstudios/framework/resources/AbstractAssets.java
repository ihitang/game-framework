package com.funfoxstudios.framework.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * Created by nikolay on 12/13/15.
 */
public abstract class AbstractAssets implements Assets {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// FIELDS
	// ===============================================================
	protected AssetManager mAssetManager;

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public AbstractAssets() {
		mAssetManager = createAssetManager();
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	public synchronized void dispose() {
		mAssetManager.dispose();
	}

	@Override
	public synchronized AssetManager getAssetManager() {
		return mAssetManager;
	}

	@Override
	public synchronized <T> T getAsset(String fileName) {
		return mAssetManager.get(fileName);
	}

	@Override
	public synchronized float getProgress() {
		//Get the progress percentage from the asset manager
		float progress = mAssetManager.getProgress();
		mAssetManager.update();
		return progress * 100;
	}

	@Override
	public synchronized void unloadResources(String resourcePath) {
		try {
			mAssetManager.unload(resourcePath);
		} catch (GdxRuntimeException ignored) {
		}
	}

	// ===============================================================
	// METHODS
	// ===============================================================
	protected AssetManager createAssetManager() {
		return new AssetManager();
	}

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}
