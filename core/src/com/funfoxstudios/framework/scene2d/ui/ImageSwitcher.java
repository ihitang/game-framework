package com.funfoxstudios.framework.scene2d.ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Pools;

public class ImageSwitcher extends Image {
	private final Drawable animationOn;
	private final Drawable animationOff;
	boolean isChecked, isDisabled;
	private ClickListener clickListener;

	public ImageSwitcher(Drawable animationOn, Drawable animationOff, boolean isChecked) {
		super(isChecked ? animationOff : animationOn);
		this.animationOn = animationOn;
		this.animationOff = animationOff;

		initialize();
		setChecked(isChecked);
	}

	private void initialize() {
		setTouchable(Touchable.enabled);
		addListener(clickListener = new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (isDisabled()) return;
				setChecked(!isChecked);
			}
		});
	}

	public boolean isDisabled() {
		return isDisabled;
	}

	/**
	 * Toggles the checked state.
	 */
	public void toggle() {
		setChecked(!isChecked);
	}

	/**
	 * When true, the button will not toggle {@link #isChecked()} when clicked and will not fire a {@link ChangeListener.ChangeEvent}.
	 */
	public void setDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public boolean isChecked() {
		return isChecked;
	}

	void setChecked(boolean isChecked) {
		if (this.isChecked == isChecked) {
			return;
		}
		this.isChecked = isChecked;

		if (this.isChecked) {
			setDrawable(animationOff);
		} else {
			setDrawable(animationOn);
		}
	}
}
