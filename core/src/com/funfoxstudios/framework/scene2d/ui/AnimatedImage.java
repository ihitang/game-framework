package com.funfoxstudios.framework.scene2d.ui;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class AnimatedImage extends Image {
	protected Animation animation = null;
	private float stateTime = 0;
	private int currentFrameId;
	private OnAnimationFinish mOnAnimationFinish;
	private boolean animationStopped;

	public AnimatedImage(Animation animation) {
		super(animation.getKeyFrame(0));
		this.animation = animation;
		animationStopped = false;
	}

	public void setOnAnimationFinishListener(OnAnimationFinish onAnimationFinish) {
		mOnAnimationFinish = onAnimationFinish;
	}

	public Animation getAnimation() {
		return animation;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
		stop();
	}

	@Override
	public void act(float delta) {
		if (!animationStopped) {
			stateTime += delta;
			updateRegion(animation.getKeyFrame(stateTime));
		}

		if (mOnAnimationFinish != null) {
			currentFrameId = animation.getKeyFrameIndex(stateTime);
			if (currentFrameId >= animation.getKeyFrames().length - 1) {
				mOnAnimationFinish.onFinish();
			}
		}
		super.act(delta);
	}

	private void updateRegion(TextureRegion region) {
		((TextureRegionDrawable) getDrawable()).setRegion(region);
	}

	public void stop() {
		stop(0.0f);
	}

	public void stop(float time) {
		animationStopped = true;
		stateTime = time;
	}

	public void start() {
		animationStopped = false;
	}

	public void stopAnimationOnFrame(float time) {
		updateRegion(animation.getKeyFrame(time));
		stop(time);
	}

	public void removeOnAnimationFinishListener() {
		mOnAnimationFinish = null;
	}

	public interface OnAnimationFinish {
		void onFinish();
	}
}