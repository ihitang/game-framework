package com.funfoxstudios.framework;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.funfoxstudios.framework.levels.LevelManager;
import com.funfoxstudios.framework.resources.Assets;
import com.funfoxstudios.framework.settings.Settings;

import java.util.List;

public interface IGame {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// METHODS
	// ===============================================================
	void loadScreen(int screenId);

	Settings getSettings();

	Assets getAssets();

	float getWordWidth();

	float getWordHeight();

	SoundController getSoundController();

	LevelManager getLevelManager();

	int getCurrentScreenId();

	SpriteBatch getBatch();
}
