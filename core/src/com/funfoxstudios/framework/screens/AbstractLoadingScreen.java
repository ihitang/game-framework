package com.funfoxstudios.framework.screens;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.funfoxstudios.framework.IGame;
import com.funfoxstudios.framework.core.background.SimpleBackground;
import com.funfoxstudios.framework.resources.Assets;

public class AbstractLoadingScreen extends Abstract2DScreen implements InputProcessor {

	// ===============================================================
	// CONSTANTS
	// ===============================================================
	private static final String TAG = AbstractLoadingScreen.class.getSimpleName();

	// ===============================================================
	// FIELDS
	// ===============================================================
	private Texture mTexture;
	private float mDuration;
	private float mTime;

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public AbstractLoadingScreen(int screenId, IGame game, SpriteBatch spriteBatch, Texture splashLogo, int nextScreenId, float durationSec, float wordWidth, float wordHeight) {
		super(screenId, game, spriteBatch, wordWidth, wordHeight);
		mTexture = splashLogo;
		mDuration = durationSec;
		setNextScreen(nextScreenId);
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	protected void updateScreen(int screenState, float delta) {
		super.updateScreen(screenState, delta);
		mTime += delta;
	}

	@Override
	protected void onStartLoading(Assets assets) {
		mTime = 0.0f;
		setBackground(new SimpleBackground(mTexture, 0.0f, 0.0f, getWordWidth(), getWordHeight()));
	}

	@Override
	protected void renderScreen(int screenState, float delta) {
		// nothing to do, splash image sets as background
	}

	@Override
	protected boolean onLoading(float progress, float delta) {
		return mGame.getAssets().getAssetManager().update() && mTime > mDuration;
	}

	@Override
	protected void onLoadingCompleted(boolean isInitialized, Assets assets, float wordWidth, float wordHeight) {
		mGame.loadScreen(getNextScreenId());
	}

	@Override
	protected void onBackButtonPress(int keyCode) {
		// nothing to do
	}

	// ===============================================================
	// METHODS
	// ===============================================================

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}
