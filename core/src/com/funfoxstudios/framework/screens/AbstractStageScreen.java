package com.funfoxstudios.framework.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.funfoxstudios.framework.IGame;
import com.funfoxstudios.framework.core.background.IBackground;
import com.funfoxstudios.framework.resources.Assets;

public abstract class AbstractStageScreen extends Stage implements Screen, InputProcessor {

	// ===============================================================
	// CONSTANTS
	// ===============================================================
	public static final int STATE_LOADING = 0;
	public static final int STATE_LOADED = 1;

	// ===============================================================
	// FIELDS
	// ===============================================================
	protected IGame mGame;
	protected Assets mAsset;
	private final int mScreenId;
	private final float mWordWidth;
	private final float mWordHeight;
	private int mNextScreenId;
	private int mPreviousScreen;
	private IBackground mBackground;
	private Vector3 mTouchPoint = new Vector3();
	private int mScreenState;
	private InputMultiplexer mMultiplexer = new InputMultiplexer();

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public AbstractStageScreen(Viewport viewport, int screenId, IGame game, float wordWidth, float wordHeight) {
		super(viewport);
		mGame = game;
		mAsset = mGame.getAssets();
		mScreenId = screenId;
		mWordWidth = wordWidth;
		mWordHeight = wordHeight;
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================
	public AbstractStageScreen setNextScreen(int nextScreenId) {
		mNextScreenId = nextScreenId;
		return this;
	}

	public int getNextScreenId() {
		return mNextScreenId;
	}

	public AbstractStageScreen setPreviousScreen(int previousScreen) {
		mPreviousScreen = previousScreen;
		return this;
	}

	public int getPreviousScreenId() {
		return mPreviousScreen;
	}

	protected Vector3 getTouchPoint() {
		getCamera().unproject(mTouchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0.0f));
		return mTouchPoint;
	}

	public float getWordWidth() {
		return getViewport().getScreenWidth();
	}

	public float getWordHeight() {
		return getViewport().getScreenHeight();
	}

	public void setBackground(IBackground background) {
		mBackground = background;
	}

	protected void setupInputProcessor() {
		Gdx.input.setInputProcessor(this);
	}

	private void clearInputProcessor() {
		Gdx.input.setInputProcessor(null);
	}

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	public void resize(int width, int height) {
		getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		setupInputProcessor();
		setState(STATE_LOADING);
		mAsset.loadScreenResources(mScreenId);
		onStartLoading(mGame.getAssets());
	}

	@Override
	public void hide() {
		clearInputProcessor();
		mAsset.unloadScreenResources(mScreenId);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		renderState(mScreenState, delta);
	}

	@Override
	public boolean keyDown(int keycode) {
		super.keyDown(keycode);

		if (keycode == Input.Keys.BACK) {
			this.onBackButtonPress(keycode);
			return true;
		}

		if (keycode == Input.Keys.ESCAPE) {
			this.onBackButtonPress(keycode);
			return true;
		}
		return false;
	}

	@Override
	public void dispose() {
		super.dispose();
		mAsset.unloadScreenResources(mScreenId);
	}

	// ===============================================================
	// METHODS
	// ===============================================================
	protected void drawBackground(float delta) {
		if (mBackground != null) {
			mBackground.draw(getBatch(), delta);
		}
	}

	protected void renderState(int screenState, float delta) {
		updateScreen(delta);
		drawBackground(delta);


		draw();

		if (screenState == STATE_LOADING) {
			if (onLoading(mAsset.getProgress(), delta)) {
				setState(STATE_LOADED);
				onLoadingCompleted(mAsset, mWordWidth, mWordHeight);
			}
		}
	}

	/**
	 * Assets loading started. Setup background, start showing progress bar, etc. {@link AbstractStageScreen#onLoading(float, float)}
	 */
	protected abstract void onStartLoading(Assets assets);

	/**
	 * Update a screen logic here
	 */
	protected abstract void updateScreen(float delta);

	/**
	 * Assets Loading in progress
	 *
	 * @param progress value from 0 to 100
	 */
	protected abstract boolean onLoading(float progress, float delta);

	/**
	 * Assets loaded trigger.
	 * A good place to create UI elements.
	 */
	protected abstract void onLoadingCompleted(Assets assets, float wordWidth, float wordHeight);

	/**
	 * Handle back button click
	 */
	protected abstract void onBackButtonPress(int keyCode);

	public void setState(int state) {
		this.mScreenState = state;
	}

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}
