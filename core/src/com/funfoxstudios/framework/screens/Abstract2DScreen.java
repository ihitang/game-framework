package com.funfoxstudios.framework.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.funfoxstudios.framework.IGame;
import com.funfoxstudios.framework.core.background.ITouchBackground;
import com.funfoxstudios.framework.core.entity.IEntity;
import com.funfoxstudios.framework.core.entity.listeners.OnClickListener;
import com.funfoxstudios.framework.resources.Assets;
import com.funfoxstudios.framework.utils.OverlapTester;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Abstract2DScreen extends ScreenAdapter implements Screen, InputProcessor, GestureDetector.GestureListener {

	// ===============================================================
	// CONSTANTS
	// ===============================================================
	public static final int STATE_LOADING = 0;
	public static final int STATE_LOADED = 1;

	// ===============================================================
	// FIELDS
	// ===============================================================
	protected IGame mGame;
	protected Assets mAsset;
	protected SpriteBatch mSpriteBatch;
	private final int mScreenId;
	private final float mWordWidth;
	private final float mWordHeight;
	private OrthographicCamera mCamera;
	private Viewport mViewport;
	private int mNextScreenId;
	private int mPreviousScreen;
	private ITouchBackground mBackground;
	private Vector3 mTouchPoint = new Vector3();
	private ArrayList<IEntity> mEntities = new ArrayList<IEntity>();
	private InputMultiplexer mMultiplexer;
	private int mScreenState;
	private boolean mIsInitialized;
	private ArrayList<StageHolder> mStages = new ArrayList<StageHolder>();
	private boolean mIsGestureDetectorEnabled;
	private Map<String, Object> mBundle;

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================
	public Abstract2DScreen(int screenId, IGame game, SpriteBatch spriteBatch, float wordWidth, float wordHeight) {
		mGame = game;
		mAsset = mGame.getAssets();
		mScreenId = screenId;
		mWordWidth = wordWidth;
		mWordHeight = wordHeight;

		mCamera = createCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		mCamera.position.set(mWordWidth / 2.0f, mWordHeight / 2.0f, 0.0f);
		mCamera.update();
		mViewport = createViewport(mWordWidth, mWordHeight);

		mSpriteBatch = spriteBatch;
	}

	public Abstract2DScreen(int screenId, IGame game, SpriteBatch spriteBatch, float wordWidth, float wordHeight, boolean isGestureDetectorEnabled) {
		this(screenId, game, spriteBatch, wordWidth, wordHeight);
		mIsGestureDetectorEnabled = isGestureDetectorEnabled;
	}

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================
	public Abstract2DScreen setNextScreen(int nextScreenId) {
		mNextScreenId = nextScreenId;
		return this;
	}

	public int getNextScreenId() {
		return mNextScreenId;
	}

	public Abstract2DScreen setPreviousScreen(int previousScreen) {
		mPreviousScreen = previousScreen;
		return this;
	}

	public int getPreviousScreenId() {
		return mPreviousScreen;
	}

	protected Vector3 getTouchPoint() {
		mCamera.unproject(mTouchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0.0f));
		return mTouchPoint;
	}

	public float getWordWidth() {
		return mWordWidth;
	}

	public float getWordHeight() {
		return mWordHeight;
	}

	public void addEntity(IEntity pEntity) {
		if (pEntity != null) {
			mEntities.add(pEntity);
		}
	}

	protected IEntity getEntity(int id) {
		return mEntities.get(id);
	}

	public void setBackground(ITouchBackground background) {
		mBackground = background;
	}

	protected void setupInputProcessor(ArrayList<StageHolder> stages) {
		if (stages == null) {
			return;
		}

		mMultiplexer = new InputMultiplexer();
		mMultiplexer.addProcessor(this);

		for (int i = stages.size() - 1; i >= 0; i--) {
			mMultiplexer.addProcessor(stages.get(i).stage);
		}

		if (mIsGestureDetectorEnabled) {
			mMultiplexer.addProcessor(new GestureDetector(this));
		}

		Gdx.input.setInputProcessor(mMultiplexer);
		Gdx.input.setCatchBackKey(true);
	}

	protected void clearInputProcessor() {
		if (mMultiplexer != null) {
			mMultiplexer.clear();
		}
		Gdx.input.setInputProcessor(null);
	}

	protected boolean isInitialized() {
		return mIsInitialized;
	}

	protected void setState(int state) {
		this.mScreenState = state;
	}

	protected void addStage(Stage stage) {
		addStage(stage, true);
	}

	protected void addStage(Stage stage, boolean automaticallyUpdateAndDraw) {
		if (stage == null) {
			throw new IllegalArgumentException("stage == null");
		}

		mStages.add(new StageHolder(stage, automaticallyUpdateAndDraw));
	}

	protected Viewport getViewport() {
		return mViewport;
	}

	public int getScreenId() {
		return mScreenId;
	}

	protected ArrayList<StageHolder> getStages() {
		return mStages;
	}

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		mViewport.update(width, height);
		resize(mStages, width, height);
	}

	@Override
	public void show() {
		super.show();
		clearInputProcessor();
		setState(STATE_LOADING);
		onStartLoading(mGame.getAssets());
		mAsset.loadScreenResources(mScreenId);
	}

	@Override
	public void hide() {
		super.hide();
		mAsset.unloadScreenResources(mScreenId);
		clearStages();
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		renderState(mScreenState, delta);
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Input.Keys.BACK) {
			this.onBackButtonPress(keycode);
			return true;
		}

		if (keycode == Input.Keys.ESCAPE) {
			this.onBackButtonPress(keycode);
			return true;
		}
		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return actionClick();
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return actionClick();
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public void dispose() {
		super.dispose();
		clearStages();
	}

	// ===============================================================
	// METHODS
	// ===============================================================
	protected Viewport createViewport(float wordWidth, float wordHeight) {
		return new FillViewport(wordWidth, wordHeight, mCamera);
	}

	protected OrthographicCamera createCamera(float screenWidth, float screenHeight) {
		return new OrthographicCamera(screenWidth, screenHeight);
	}

	protected void drawEntities(float delta, OrthographicCamera camera, ArrayList<IEntity> entities) {
		camera.update();
		mSpriteBatch.setProjectionMatrix(camera.combined);
		for (int i = 0; i < entities.size(); i++) {
			entities.get(i).draw(mSpriteBatch);
		}
	}

	protected void drawBackground(float delta) {
		if (mBackground != null) {
			mBackground.draw(mSpriteBatch, delta);
		}
	}

	protected void renderState(int screenState, float delta) {
		updateScreen(screenState, delta);
		drawBackground(delta);
		drawEntities(delta, mCamera, mEntities);
		renderScreen(mScreenState, delta);
	}

	private boolean actionClick() {
		return handleOnClick(mEntities, OnClickListener.Types.Click, getTouchPoint());
	}

	private boolean handleOnClick(ArrayList<IEntity> entities, OnClickListener.Types type, Vector3 touchPoint) {
		if (entities.size() > 0) {
			for (int i = 0; i < entities.size(); i++) {
				if (entities.get(i).getVisible()) {

					if (entities.get(i).getChildren().size() > 0) {
						for (int childId = 0; childId < entities.get(i).getChildren().size(); childId++) {
							IEntity child = entities.get(i).getChild(childId);
							if (child.isClickable()) {
								if (OverlapTester.pointInRectangle(child, touchPoint)) {
									switch (type) {
										case Click:
											child.onClick();
											break;
										case TouchDown: {
											child.onTouchDown();
											break;
										}
									}
									return true;
								}
							}
						}
					}
					if (entities.get(i).isClickable()) {
						if (OverlapTester.pointInRectangle(entities.get(i), touchPoint)) {
							switch (type) {
								case Click:
									entities.get(i).onClick();
									break;
								case TouchDown: {
									entities.get(i).onTouchDown();
									break;
								}
							}
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	protected void clearStages() {
		if (mStages == null) {
			return;
		}
		for (int i = 0; i < mStages.size(); i++) {
			mStages.get(i).stage.dispose();
		}
		mStages.clear();
	}

	/**
	 * Assets loading started. Setup background, show progress bar, etc. {@link Abstract2DScreen#onLoading(float, float)}
	 */
	protected abstract void onStartLoading(Assets assets);

	/**
	 * Update a screen logic here
	 */
	protected void updateScreen(int screenState, float delta) {
		switch (screenState) {
			case STATE_LOADING:
				if (onLoading(mAsset.getProgress(), delta)) {
					setState(STATE_LOADED);
					onLoadingCompleted(mIsInitialized, mAsset, mWordWidth, mWordHeight);
					setupInputProcessor(mStages);
					mIsInitialized = true;
					return;
				}
				break;
			case STATE_LOADED:
				break;
		}
	}

	/**
	 * Render the screen based on screen's state
	 */
	protected void renderScreen(int screenState, float delta) {
		switch (screenState) {
			case STATE_LOADED:
				updateAndDraw(mStages);
				break;
		}
	}

	protected void updateAndDraw(ArrayList<StageHolder> stages) {
		for (int i = 0; i < stages.size(); i++) {
			StageHolder holder = stages.get(i);
			if (holder.automaticallyUpdateAndDraw) {
				holder.stage.act();
				holder.stage.draw();
			}
		}
	}

	private void resize(ArrayList<StageHolder> stages, int width, int height) {
		for (int i = 0; i < stages.size(); i++) {
			stages.get(i).stage.getViewport().update(width, height);
		}
	}

	/**
	 * Assets Loading in progress
	 *
	 * @param progress value from 0 to 100
	 * @return true if resources loaded
	 */
	protected boolean onLoading(float progress, float delta) {
		return mGame.getAssets().getProgress() == 100;
	}

	/**
	 * Assets loaded trigger.
	 * A good place to create UI elements.
	 */
	protected abstract void onLoadingCompleted(boolean isInitialized, Assets assets, float wordWidth, float wordHeight);

	/**
	 * Handle back button click (ESC or Android Back button)
	 */
	protected abstract void onBackButtonPress(int keyCode);

	private class StageHolder {
		Stage stage;
		boolean automaticallyUpdateAndDraw;

		public StageHolder(Stage stage, boolean automaticallyUpdateAndDraw) {
			this.stage = stage;
			this.automaticallyUpdateAndDraw = automaticallyUpdateAndDraw;
		}
	}

	protected void save(String key, String value){
		mBundle.put(key, value);
	}

	protected void save(String key, Integer value){
		mBundle.put(key, value);
	}

	protected void save(String key, Double value){
		mBundle.put(key, value);
	}

	protected void save(String key, Long value){
		mBundle.put(key, value);
	}

	protected <T> T getValue(String key, T objectType){
		return (T) mBundle.get(key);
	}

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}
