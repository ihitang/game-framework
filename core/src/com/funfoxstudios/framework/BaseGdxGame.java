package com.funfoxstudios.framework;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.funfoxstudios.framework.levels.LevelManager;
import com.funfoxstudios.framework.resources.Assets;
import com.funfoxstudios.framework.settings.Settings;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseGdxGame<T> extends Game implements IGame {
	private static Settings mSettings;
	private static Assets mAssets;
	private LevelManager<T> mLevelManager;
	private Map<Integer, Screen> mScreens = new HashMap<Integer, Screen>();
	private float mWordWidth;
	private float mWordHeight;
	private int mCurrentScreenId;
	private SoundController mSoundController;
	private SpriteBatch mBatch;

	public BaseGdxGame(Settings settings, float wordWidth, float wordHeight) {
		mSettings = settings;
		mWordWidth = wordWidth;
		mWordHeight = wordHeight;
	}

	@Override
	public void create() {
		mLevelManager = createLevelManager();
		mAssets = createAssets(mLevelManager);
		mSoundController = createSoundController(mSettings);
		mBatch = new SpriteBatch();
	}

	@Override
	public Settings getSettings() {
		return mSettings;
	}

	@Override
	public Assets getAssets() {
		return mAssets;
	}

	@Override
	public SoundController getSoundController() {
		return mSoundController;
	}

	@Override
	public void loadScreen(int screenId) {
		Screen screen = mScreens.get(screenId);
		if (screen == null) {
			screen = addScreen(screenId, mBatch, mWordWidth, mWordHeight);
			mScreens.put(screenId, screen);
		}

		mCurrentScreenId = screenId;

		setScreen(screen);
	}

	@Override
	public float getWordWidth() {
		return mWordWidth;
	}

	@Override
	public float getWordHeight() {
		return mWordHeight;
	}

	@Override
	public LevelManager getLevelManager() {
		return mLevelManager;
	}

	@Override
	public int getCurrentScreenId(){
		return mCurrentScreenId;
	}

	@Override
	public SpriteBatch getBatch(){
		return mBatch;
	}

	@Override
	public void dispose() {
		super.dispose();
		mAssets.dispose();
		mBatch.dispose();
	}

	protected abstract Assets createAssets(LevelManager<T> levelManager);

	protected abstract LevelManager<T> createLevelManager();

	protected abstract Screen addScreen(int screenId, SpriteBatch spriteBatch, float wordWidth, float wordHeight);

	protected abstract SoundController createSoundController(Settings settings);
}
