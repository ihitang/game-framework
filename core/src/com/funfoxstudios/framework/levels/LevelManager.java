package com.funfoxstudios.framework.levels;

import java.util.List;

/**
 * Holds all information about levels, assets, icons, level's id.
 * Presents ability to set current level id.
 * Should works in pair with asset manager and game screen.
 *
 * @see com.badlogic.gdx.Screen
 * @see com.funfoxstudios.framework.screens.Abstract2DScreen
 * @see com.funfoxstudios.framework.resources.Assets
 */
public interface LevelManager<T> {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// METHODS
	// ===============================================================

	/**
	 * Returns a list with textures paths
	 */
	List<String> getLevelsIcons();

	/**
	 * @return T some level description. Should include assets, level id, etc.
	 */
	T getLevel(int levelId);

	/**
	 * Level manager should hold information about current level id.
	 * @return boolean, if the level exist and can be loaded
	 */
	boolean setLevel(int levelId);

	/**
	 * Level manager should hold information about current level id.
	 * the method increases the current level id by 1.
	 * @return boolean, if the next level exist and can be loaded
	 */
	boolean setNextLevel();

	/**
	 * @return boolean, if the next level exist and can be loaded
	 */
	boolean isNextLevelExist();

	/**
	 * Returns current level id
	 *
	 * @see LevelManager#getLevel(int)
	 */
	int getCurrentLevel();
}
