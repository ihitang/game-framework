package com.funfoxstudios.framework.settings;

public interface Settings {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// METHODS
	// ===============================================================
	void save(String key, String value);

	String getString(String key);

	void addListener(OnSettingsChange listener);

	void removeListener(OnSettingsChange listener);

	void clearAllListeners();

	interface OnSettingsChange {

		void onChange(String key, String value);

	}
}
