package com.funfoxstudios.framework.settings;

import java.util.ArrayList;

public abstract class AbstractSettings implements Settings {

	// ===============================================================
	// CONSTANTS
	// ===============================================================

	// ===============================================================
	// FIELDS
	// ===============================================================
	private ArrayList<OnSettingsChange> mListeners = new ArrayList<OnSettingsChange>();

	// ===============================================================
	// CONSTRUCTORS
	// ===============================================================

	// ===============================================================
	// GET AND SET METHODS
	// ===============================================================

	// ===============================================================
	// SUPER OR OVERRIDE METHODS
	// ===============================================================
	@Override
	public void save(String key, String value) {
		updateListeners(key, value);
	}

	@Override
	public void addListener(OnSettingsChange listener) {
		mListeners.add(listener);
	}

	@Override
	public void removeListener(OnSettingsChange listener) {
		mListeners.remove(listener);
	}

	@Override
	public void clearAllListeners() {
		mListeners.clear();
	}

	// ===============================================================
	// METHODS
	// ===============================================================
	private void updateListeners(String key, String value) {
		for (int i = 0; i < mListeners.size(); i++) {
			mListeners.get(i).onChange(key, value);
		}
	}

	// ===============================================================
	// OTHER CLASSES
	// ===============================================================

}
